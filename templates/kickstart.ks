# {{ ansible_managed }}
{% set version_number = version | string | regex_search('^([0-9]+)', '\\1') | first | int %}

# debug:
#   family: {{ family }}
#   version: {{ version }}
#   version_number: {{ version_number }}

text

lang {{ lang }}.UTF-8
keyboard {{ keyboard }}

network --onboot yes --bootproto {{ bootproto }}{% if bootproto == 'static' %} --ip {{ network.ip | ipaddr('address') }} --netmask {{ network.ip | ipaddr('netmask') }}{% for n in nameservers %} --nameserver {{ n }}{% endfor %} --gateway {{ network.gateway }}{% endif %} --noipv6 --hostname {{ fqdn }}


timezone --utc {{ timezone }}
rootpw --iscrypted {{ root_pw |password_hash('sha512', hash) }}
{% if sshkeys is defined and family == 'Fedora' %}
{% for key in sshkeys %}
sshkey --username=root "{{ key }}"
{% endfor %}
{% endif %}
{% if (family == 'Fedora' and (version == 'rawhide' or version_number >= 29)) or ((family == 'CentOS' or family == 'RHEL') and version_number >= 8) %}
# we can use authselect instead of authconfig but the default seems fine
{% else %}
authconfig --enableshadow --passalgo=sha512 --enablefingerprint
{% endif %}


ignoredisk --only-use={{ boot_disk }}
bootloader --location=mbr --boot-drive={{ boot_disk }}
zerombr
clearpart --all --drive={{ boot_disk }}
part /boot --fstype={{ filesystem }} --size=500 --asprimary --ondisk={{ boot_disk }}
part pv.01 --grow --size=5000 --ondisk={{ boot_disk }}
volgroup vg_root_{{ fqdn }} pv.01
logvol / --vgname=vg_root_{{ fqdn }} --fstype={{ filesystem }} --grow --size=5000 --maxsize=8000 --name=root

services --enabled=sshd
# ostree do no have firewalld
{% if not ostree %}
firewall --service=ssh
{% endif %}

{% if ostree and family == 'Fedora' %}
ostreesetup --nogpg --osname="fedora-atomic" --remote="fedora-atomic-{{ version }}" --url="{{ ostree_base_url | default('https://dl.fedoraproject.org/pub/fedora/linux/atomic/') }}/repo" --ref="fedora/{{ version }}/{{ architecture }}/atomic-host"
{% endif %}

reboot

{% if not ostree %}
%packages

openssh-server
{% if (family == 'Fedora' and (version == 'rawhide' or version_number >= 29)) or ((family == 'CentOS' or family == 'RHEL') and version_number >= 8) %}
# Ansible is now able to detect the install Python version
#python3
# not installed by default in EL9+ and F35+ (at least)
policycoreutils-python-utils
{% else %}
python
{% endif %}
{% if extra_packages is defined %}
{% for p in extra_packages %}
{{ p }}
{% endfor %}
{% endif %}
%end
{% endif %}

%post --log=/root/ansible_post.log
{% if sshkeys is defined %}
{% if family == 'CentOS' or family == 'RHEL' %}
mkdir -p /root/.ssh/
chmod 700 /root/.ssh/
{% for key in sshkeys %}
echo "{{ key }}" >> /root/.ssh/authorized_keys
{% endfor %}
restorecon -Rv /root/.ssh/
{% endif %}
config="/etc/ssh/sshd_config"
if grep -q '^PermitRootLogin ' $config; then
    sed 's/^PermitRootLogin .*/PermitRootLogin without-password/' -i $config
else
    echo "PermitRootLogin without-password" >> $config
fi;

{% endif %}

{% if postinstall is defined %}
{{ postinstall }}
{% endif %}
%end
